/*
 * tiva_headers.h
 *
 *  Created on: 13.03.2018
 *      Author: mayerflo
 */

//JUST A COMBINATION OF ALL NEEDED TIVA HEADERS

#ifndef TIVA_HEADERS_H_
#define TIVA_HEADERS_H_

#include "stdint.h"
#include "stdbool.h"

#include "/Library/ti/tiva_ware/inc/hw_gpio.h"
#include "/Library/ti/tiva_ware/inc/hw_types.h"
#include "/Library/ti/tiva_ware/inc/hw_memmap.h"
#include "/Library/ti/tiva_ware/inc/tm4c123gh6pm.h"
#include "/Library/ti/tiva_ware/inc/hw_i2c.h"

#include "/Library/ti/tiva_ware/driverlib/sysctl.h"
#include "/Library/ti/tiva_ware/driverlib/gpio.h"
#include "/Library/ti/tiva_ware/driverlib/pin_map.h"
#include "/Library/ti/tiva_ware/driverlib/timer.h"
#include "/Library/ti/tiva_ware/driverlib/interrupt.h"
#include "/Library/ti/tiva_ware/driverlib/pwm.h"
#include "/Library/ti/tiva_ware/driverlib/ssi.h"
#include "/Library/ti/tiva_ware/driverlib/uart.h"
#include "/Library/ti/tiva_ware/driverlib/i2c.h"
#include "/Library/ti/tiva_ware/driverlib/adc.h"
#include "/Library/ti/tiva_ware/driverlib/comp.h"


#endif /* TIVA_HEADERS_H_ */
