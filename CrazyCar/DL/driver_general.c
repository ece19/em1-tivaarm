/*
 * driver_general.c
 *
 *  Created on: 22.10.2017
 *      Author: mayerflo
 */

#include "tiva_headers.h"

#include "driver_general.h"
#include "driver_aktorik.h"
#include "driver_LCD.h"
#include "driver_icm.h"
#include "driver_sonic.h"

#include "../HAL/hal_gpio.h"

void driver_INIT(void)
{
    driverESCInit();
    Driver_LCD_Init();
    Driver_InitICM();
    sonicInit();
}
