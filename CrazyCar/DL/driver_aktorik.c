#include "tiva_headers.h"
#include "driver_aktorik.h"

// local prototype functions ------------------------
void driverThrottleCalibrate(short speed, char count_time) ;
// --------------------------------------------------

// global variables ---------------------------------
short pulse_counter = 0;
calibration cal = RUNNING;
// --------------------------------------------------

void driverESCInit(){
    driverThrottleInit();
    driverSteeringInit();
}

/**
 * Set steering angle by given value from -100 (max left) to 100 (max right)
 */
void driverSetSteering(signed char steer_val){
    if (steer_val > STEERING_RANGE_R) {
        PWMPulseWidthSet(PWM1_BASE, PWM_OUT_2 , ST_MAXRIGHT);
    } else if (steer_val < STEERING_RANGE_L) {
        PWMPulseWidthSet(PWM1_BASE, PWM_OUT_2 , ST_MAXLEFT);
    } else {
        PWMPulseWidthSet(PWM1_BASE, PWM_OUT_2 , (ST_MIDDLE + steer_val * 8));
    }
}

/**
 * Set throttle speed by given value from -100 (max reverse) to 100 (max forward)
 */
void driverSetThrottle(signed char throttle_val){
    if (throttle_val > THROTTLE_RANGE_F) {
        PWMPulseWidthSet(PWM1_BASE, PWM_OUT_3 , MaxFPW);
    } else if (throttle_val > 0) {
        PWMPulseWidthSet(PWM1_BASE, PWM_OUT_3 , (MinFPW + throttle_val * THROTTLE_STEP));
    } else if (throttle_val < THROTTLE_RANGE_R) {
        PWMPulseWidthSet(PWM1_BASE, PWM_OUT_3 , MaxRPW);
    } else if (throttle_val < 0) {
        PWMPulseWidthSet(PWM1_BASE, PWM_OUT_3 , (MinRPW + throttle_val * THROTTLE_STEP));
    } else {
        PWMPulseWidthSet(PWM1_BASE, PWM_OUT_3 , MaxBreak);
    }
}

void driverMatlabDrive(){

}

/**
 * Initialize steering
 */
void driverSteeringInit(){
    PWMPulseWidthSet(PWM1_BASE, PWM_OUT_2 , ST_MIDDLE);
}

/**
 * Initial throttle calibration
 */
void driverThrottleInit(){
    driverThrottleCalibrate(MaxRPW, 140);
    driverThrottleCalibrate(MinRPW, 140);
    driverThrottleCalibrate(MinFPW, 140);
    driverThrottleCalibrate(MaxFPW, 140);
    driverThrottleCalibrate(MaxBreak, 20);
    cal = END;
}

/**
 * Set given speed for 140 pulses (throttle calibration)
 */
void driverThrottleCalibrate(short speed, char count_time) {
    pulse_counter = 0;
    while (pulse_counter < count_time){
        PWMPulseWidthSet(PWM1_BASE, PWM_OUT_3 , speed);
    };

}
