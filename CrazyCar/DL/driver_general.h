/*
 * driver_general.h
 *
 *  Created on: 22.10.2017
 *      Author: mayerflo
 */

#ifndef DL_DRIVER_GENERAL_H_
#define DL_DRIVER_GENERAL_H_

void driver_INIT(void);

typedef struct {
    float gyroBias[3];
    float accelBias[3];
    float aRes;
    float gRes;
    float mRes;
    float f_MPUData[7];
    float f_MPUAngle[3]; //Initialized after MPU init
    float f_MPUPos[3]; //Initialized after MPU init
    uint8_t Ascale;
    uint8_t Gscale;
    uint8_t Mscale;
    uint8_t gyroDriftcnt;
    float gyroDrift;
} MPUSetData;

#endif /* DL_DRIVER_GENERAL_H_ */
