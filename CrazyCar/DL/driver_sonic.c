/*
 * driver_sonic.c
 *
 *  Created on: 02.06.2021
 *      Author: tim
 */

#include "tiva_headers.h"
#include "driver_sonic.h"
#include "HAL/hal_gpio.h"
#include "HAL/hal_ucs.h"

enum sonic_state {ENABLE_TRANSMIT, AWAIT_TRANSMIT, DISABLE_TRANSMIT, RECEIVE} current_state;

short send_counter;
short sonic_general_counter;
char sonic_receive_distance;

void sonicInit(){
    current_state = ENABLE_TRANSMIT;
    sonic_receive_distance = 0;
}

void sonicStateMachine(){
    switch(current_state){

    case ENABLE_TRANSMIT:
        send_counter = 0;
        sonic_general_counter = 0;
        PWMPulseWidthSet(PWM1_BASE, PWM_OUT_1, 31);
        PWMOutputState(PWM1_BASE, PWM_OUT_1_BIT, true);
        GPIOPinWrite(GPIO_PORTD_BASE, TX_ENABLE, TX_ENABLE);

        current_state = AWAIT_TRANSMIT;
        break;

    case AWAIT_TRANSMIT:
        if(send_counter >= SONIC_MAX_TRANSMIT){
            current_state = DISABLE_TRANSMIT;
        }
        break;

    case DISABLE_TRANSMIT:
        GPIOPinWrite(GPIO_PORTD_BASE, TX_ENABLE, TX_ENABLE);
        PWMPulseWidthSet(PWM1_BASE, PWM_OUT_1, 0);
        PWMOutputState(PWM1_BASE, PWM_OUT_1_BIT, false);
        delay_ms(1);
        sonic_receive_distance = 0;
        current_state = RECEIVE;
        break;

    case RECEIVE:

        if(sonic_general_counter >= SONIC_MAX_PERIOD){
            send_counter = 0;
            sonic_general_counter = 0;
            current_state = ENABLE_TRANSMIT;
        }
        if(sonic_receive_distance){
            getDistanceFromTime((SONIC_MAX_PERIOD - sonic_general_counter));
            sonic_receive_distance = 0;
        }
        break;
    default:
        break;
    }
}

void getDistanceFromTime(short time_diff){



}










