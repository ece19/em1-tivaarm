/*
 * driver_sonic.h
 *
 *  Created on: 02.06.2021
 *      Author: tim
 */

#ifndef DL_DRIVER_SONIC_H_
#define DL_DRIVER_SONIC_H_

#define SONIC_MAX_PERIOD 666
#define SONIC_MAX_TRANSMIT 8
#define SONIC_PWM_DUTYCYCLE 31
#define SONIC_SPEED_CONSTANT

void sonicInit(void);
void sonicStateMachine(void);
void getDistanceFromTime(short time_diff);

#endif /* DL_DRIVER_SONIC_H_ */
