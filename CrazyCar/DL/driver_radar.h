/*
 * driver_radar.h
 *
 *  Created on: 09.11.2021
 *      Author: lukas
 */

#ifndef DL_DRIVER_RADAR_H_
#define DL_DRIVER_RADAR_H_

#define RADAR_SAW_PERIOD_MS 15
#define RADAR_SAMPLE_PERIOD_MS 12

#define RADAR_MAX_DAC_STEPS 3750
#define RADAR_MIN_DAC_STEPS 0

#define RADAR_MIN_ADC_STEPS 0
#define RADAR_MAX_ADC_STEPS 3750

#define SECOND_IN_MS 1000

#define DAC_OPT 0x0000 // normal mode

void DRIVER_RADAR_INIT();


#endif /* DL_DRIVER_RADAR_H_ */
