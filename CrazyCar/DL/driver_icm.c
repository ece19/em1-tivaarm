/*
 * driver_icm.c
 *
 *  Created on: 02.06.2021
 *      Author: lukas
 */

#include <math.h>
#include "tiva_headers.h"
#include "../HAL/hal_gpio.h"
#include "../HAL/hal_i2c.h"
#include "../HAL/hal_ucs.h"

#include "driver_icm.h"
#include "driver_general.h"

extern CarDataCom CarDataArray;
MPUSetData MPUSetDat;

void Driver_InitICM(void)
{
    uint8_t ready = 0;

    getMPUBytes(ICM20948_ADDRESS, WHO_AM_I_ICM20948, 1, &ready);
    if (ready != 0xEA) {
        return;
    }

    /// MPU SETUP
    MPUSetDat.Ascale = AFS_4G;     // AFS_2G, AFS_4G, AFS_8G, AFS_16G
    MPUSetDat.Gscale = GFS_500DPS; // GFS_250DPS, GFS_500DPS, GFS_1000DPS, GFS_2000DPS
    MPUSetDat.Mscale = MFS_16BITS; // MFS_14BITS or MFS_16BITS, 14-bit or 16-bit magnetometer resolution
    ///

    calibrateICM20948();
    delay_ms(100);
    initICM20948();
    delay_ms(100);
    getResolution();

    char i;
    for (i = 0; i < 3; i++) {
        MPUSetDat.f_MPUAngle[i] = 0;
        MPUSetDat.f_MPUPos[i] = 0;
    }

    GPIOIntEnable(GPIO_PORTB_BASE, MOTION_I2C_INT); // Activates INT-PIN on TIVA
}

void calibrateICM20948()
{
    Set_REGBANK(0);
    writeMPUByte(ICM20948_ADDRESS, PWR_MGMT_1, 0x80); //Reset
    delay_ms(200);

    // get stable time source & set gyro-x as clock source
    writeMPUByte(ICM20948_ADDRESS, PWR_MGMT_1, 0x01);
    writeMPUByte(ICM20948_ADDRESS, PWR_MGMT_2, 0x00);
    delay_ms(200);

    // Configure MPU for bias calculation
    writeMPUByte(ICM20948_ADDRESS, INT_ENABLE, 0x00);               // disable interrupts
    writeMPUByte(ICM20948_ADDRESS, FIFO_EN_1, 0x00);                // disable FIFO
    writeMPUByte(ICM20948_ADDRESS, FIFO_EN_2, 0x00);                // disable FIFO

    writeMPUByte(ICM20948_ADDRESS, USER_CTRL, 0x00);                // disable FIFO
    writeMPUByte(ICM20948_ADDRESS, USER_CTRL, 0x08);                // disable FIFO & I2C Mode

    writeMPUByte(ICM20948_ADDRESS, FIFO_RST, 0x1F);
    delay_ms(20);
    writeMPUByte(ICM20948_ADDRESS, FIFO_RST, 0x00);
    delay_ms(20);
    writeMPUByte(ICM20948_ADDRESS, FIFO_MODE, 0x1F);

    Set_REGBANK(0);
    // Configure ACCEL and GYRO for bias calculation
    writeMPUByte(ICM20948_ADDRESS, GYRO_CONFIG_1, 0x01);    // set low-pass
    writeMPUByte(ICM20948_ADDRESS, GYRO_SMPLRT_DIV, 0x00);  // set sample-rate
    writeMPUByte(ICM20948_ADDRESS, ACCEL_CONFIG, 0x01);

    // Collect MPU Data using FIFO for bias calculation
    writeMPUByte(ICM20948_ADDRESS, USER_CTRL, 0x40);     // enable FIFO
    writeMPUByte(ICM20948_ADDRESS, FIFO_EN_2, 0x1E);     // enable Gyro & Accel for FIFO
    delay_ms(35);     // wait until 480 Bytes are stored in the FIFO (1125kHz sampling rate)

    // Stop MPU
    uint8_t fifo_count_arr[] = {0, 0};
    uint16_t fifo_count = 0;

    writeMPUByte(ICM20948_ADDRESS, FIFO_EN_2, 0x00);     // disable Gyro & Accel fpr FIFO
    getMPUBytes(ICM20948_ADDRESS, FIFO_COUNTH, 2, fifo_count_arr);

    fifo_count = ((uint16_t)fifo_count_arr[0] << 8) + ((uint16_t)fifo_count_arr[1] & 0xFF);

    // calculate bias
    fifo_count /= 12;
    uint16_t i = 0;

    int32_t accel_tmp[3] = {0};
    int32_t gyro_tmp[3] = {0};

    uint8_t data[12];
    for (i = 0; i < fifo_count; i++) {
        getMPUBytes(ICM20948_ADDRESS, FIFO_R_W, 12, data);

        accel_tmp[0] += (int16_t)(((uint16_t)data[0] << 8) | data[1]);
        accel_tmp[1] += (int16_t)(((uint16_t)data[2] << 8) | data[3]);
        accel_tmp[2] += (int16_t)(((uint16_t)data[4] << 8) | data[5]);

        gyro_tmp[0] += (int16_t)(((uint16_t)data[6] << 8) | data[7]);
        gyro_tmp[1] += (int16_t)(((uint16_t)data[8] << 8) | data[9]);
        gyro_tmp[2] += (int16_t)(((uint16_t)data[10] << 8) | data[11]);
    }

    MPUSetDat.accelBias[0] = (float)accel_tmp[0]/fifo_count;
    MPUSetDat.accelBias[1] = (float)accel_tmp[1]/fifo_count;
    MPUSetDat.accelBias[2] = (float)accel_tmp[2]/fifo_count;

    MPUSetDat.gyroBias[0] = (float)gyro_tmp[0]/fifo_count;
    MPUSetDat.gyroBias[1] = (float)gyro_tmp[1]/fifo_count;
    MPUSetDat.gyroBias[2] = (float)gyro_tmp[2]/fifo_count;

    // scale bias
    for (i = 0; i < 3; i++) {
        MPUSetDat.accelBias[i] = MPUSetDat.accelBias[i] * 2.0f / 32768.0f;
    }

    for (i = 0; i < 3; i++) {
        MPUSetDat.gyroBias[i] = MPUSetDat.gyroBias[i] * 250.0f/32768.0f;
    }

    // add earth acceleration
    uint16_t earth_accel = 1;
    if (MPUSetDat.accelBias[2] > 0) {
        MPUSetDat.accelBias[2] -= earth_accel;
    } else {
        MPUSetDat.accelBias[2] += earth_accel;
    }
}

void initICM20948(void)
{
    uint8_t var_config = 0x00;

    Set_REGBANK(2);
    getMPUBytes(ICM20948_ADDRESS, GYRO_CONFIG_1, 1, &var_config);

    var_config &= 0b10011111;
    var_config |= 0b00000010 | 0b00000001 | 0b00011000; // set scale to 500dps, GYRO_FCHOICE, low pass to 51.25Hz

    writeMPUByte(ICM20948_ADDRESS, GYRO_CONFIG_1, var_config);

    getMPUBytes(ICM20948_ADDRESS, ACCEL_CONFIG, 1, &var_config);

    var_config &= 0b10011111;
    var_config |= 0b00000010 | 0b00000001 | 0b00011000; // set scale to 4G, ACCEL_FCHOICE, low pass to 50.4Hz

    writeMPUByte(ICM20948_ADDRESS, ACCEL_CONFIG, var_config);

    writeMPUByte(ICM20948_ADDRESS, ACCEL_SMPLRT_DIV_2, 0x04);   // Divide Samplerate /4
    writeMPUByte(ICM20948_ADDRESS, GYRO_SMPLRT_DIV, 0x04);      // Divide Samplerate /4

    Set_REGBANK(0);
    writeMPUByte(ICM20948_ADDRESS, INT_PIN_CFG, 0x12);          // Set Pulsewidth for Interrupt to 5ms
    writeMPUByte(ICM20948_ADDRESS, INT_ENABLE_1, 0x1);          // Enable data ready Interrupt
}

void getMPUData()
{
    uint8_t tmp_data[12];
    int16_t tmp_data_16[6];

    getMPUBytes(ICM20948_ADDRESS, ACCEL_XOUT_H, 12, tmp_data); //Read Accel/Gyro Data

    char i = 0;
    for (i = 0; i < 6; i++) {
        tmp_data_16[i] = (int16_t)(((uint16_t)tmp_data[2*i] << 8) | tmp_data[2*i + 1]);

        if (i < 3) {
            MPUSetDat.f_MPUData[i] = MPUSetDat.aRes * tmp_data_16[i] - MPUSetDat.accelBias[i];
            //CarDataArray.rf_data[i + 5] = (unsigned short)(tmp_data_16[i]);
        } else {
            MPUSetDat.f_MPUData[i] = MPUSetDat.gRes * tmp_data_16[i] - MPUSetDat.gyroBias[i - 3];
        }
    }

    calcAngle();
    calcPosition();
}

void calcAngle() {
    float x_acc, y_acc, z_ang_old;
    z_ang_old = MPUSetDat.f_MPUAngle[2];

    MPUSetDat.f_MPUAngle[0] += MPUSetDat.f_MPUData[3] * dt;
    MPUSetDat.f_MPUAngle[1] += MPUSetDat.f_MPUData[4] * dt;
    MPUSetDat.f_MPUAngle[2] += MPUSetDat.f_MPUData[5] * dt;

    if (((MPUSetDat.f_MPUAngle[2] - z_ang_old)  < 0.025f) & ((MPUSetDat.f_MPUAngle[2] - z_ang_old) > -0.025f)) {
        MPUSetDat.f_MPUAngle[2] = z_ang_old;
    }
    if ((MPUSetDat.f_MPUAngle[2] > 180)) {
        MPUSetDat.f_MPUAngle[2] = -180;
    } else if ((MPUSetDat.f_MPUAngle[2] < -180)) {
        MPUSetDat.f_MPUAngle[2] = 180;
    }

    // Complementary Filter
    x_acc = atan2(MPUSetDat.f_MPUData[1], MPUSetDat.f_MPUData[2]) * (180 / M_PI);
    MPUSetDat.f_MPUAngle[0] = MPUSetDat.f_MPUAngle[0] * 0.9f + x_acc * 0.1f;

    y_acc = atan2(MPUSetDat.f_MPUData[0], MPUSetDat.f_MPUData[2]) * (180 / M_PI);
    MPUSetDat.f_MPUAngle[1] = MPUSetDat.f_MPUAngle[1] * 0.9f + y_acc * 0.1f;

    char i = 0;
    for (i = 0; i < 3; i++) {
        CarDataArray.rf_data[i + 5] = (unsigned short)(MPUSetDat.f_MPUAngle[i] / 180.0f * 32768.0f + 32768);
    }
}

void calcPosition() {
    float x_dif, y_dif;

    x_dif = (float)CarDataArray.RPM_dir * CarDataArray.RPM_Speed * cosf(MPUSetDat.f_MPUAngle[2] * (M_PI / 180)) * dt;
    y_dif = (float)CarDataArray.RPM_dir * CarDataArray.RPM_Speed * sinf(MPUSetDat.f_MPUAngle[2] * (M_PI / 180)) * dt;

    MPUSetDat.f_MPUPos[0] += x_dif;
    MPUSetDat.f_MPUPos[1] += y_dif;

    char i;
    for (i = 0; i < 2; i++) {
        CarDataArray.rf_data[i + 8] = (unsigned short)(MPUSetDat.f_MPUPos[i] + 32768);
    }
}

void getResolution(void)
{
      switch(MPUSetDat.Mscale)
      {
        // Possible magnetometer scales (and their register bit settings) are:
        // 14 bit resolution (0) and 16 bit resolution (1)
        case MFS_14BITS:
              MPUSetDat.mRes = 10.0*4912.0/8190.0; // Proper scale to return milliGauss
              break;
        case MFS_16BITS:
              MPUSetDat.mRes = 10.0*4912.0/32760.0; // Proper scale to return milliGauss
              break;
      }
      switch(MPUSetDat.Gscale)
      {
        // Possible gyro scales (and their register bit settings) are:
        // 250 DPS (00), 500 DPS (01), 1000 DPS (10), and 2000 DPS  (11).
            // Here's a bit of an algorith to calculate DPS/(ADC tick) based on that 2-bit value:
        case GFS_250DPS:
              MPUSetDat.gRes = 250.0f/32768.0f;
              break;
        case GFS_500DPS:
              MPUSetDat.gRes = 500.0f/32768.0f;
              break;
        case GFS_1000DPS:
              MPUSetDat.gRes = 1000.0f/32768.0f;
              break;
        case GFS_2000DPS:
              MPUSetDat.gRes = 2000.0f/32768.0f;
              break;
      }
      switch(MPUSetDat.Ascale)
      {
        // Possible accelerometer scales (and their register bit settings) are:
        // 2 Gs (00), 4 Gs (01), 8 Gs (10), and 16 Gs  (11).
            // Here's a bit of an algorithm to calculate DPS/(ADC tick) based on that 2-bit value:
        case AFS_2G:
              MPUSetDat.aRes = 2.0f/32768.0f;
              break;
        case AFS_4G:
              MPUSetDat.aRes = 4.0f/32768.0f;
              break;
        case AFS_8G:
              MPUSetDat.aRes = 8.0f/32768.0f;
              break;
        case AFS_16G:
              MPUSetDat.aRes = 16.0f/32768.0f;
              break;
      }
}

void Set_REGBANK(uint8_t regbank)
{
    writeMPUByte(ICM20948_ADDRESS, REG_BANK_SEL, (regbank << 4));
    delay_ms(10);
}

