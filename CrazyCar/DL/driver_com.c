/*
 * driver_COM.c
 *
 *  Created on: 20.04.2021
 *      Author: lukas
 */

#include "tiva_headers.h"
#include "driver_com.h"
#include "driver_aktorik.h"

#include "HAL/hal_uart1.h"
#include "HAL/hal_gpio.h"

#define PAYLOAD 22

extern CarDataCom CarDataArray;

unsigned char start_id[] = {PAYLOAD, 'C', ':'};
unsigned char end_id[]  = ":E"; // {':', 'E'}

void transmitData(unsigned short* data) {
    unsigned char* transmit_buffer = (unsigned char*) data;

    UART1_Transmit(start_id, 3);
    UART1_Transmit(transmit_buffer, PAYLOAD - 4);
    UART1_Transmit(end_id, 2);
}

void recData() {
    if (CarDataArray.rf_rec[0] == 'S' && CarDataArray.rf_rec[1] == ':' && CarDataArray.rf_rec[4] == ':' && CarDataArray.rf_rec[5] == 'E') {
        CarDataArray.car_Steering = (signed char)CarDataArray.rf_rec[2] - 100;


        if (CarDataArray.rf_rec[3] <= 100) {
            CarDataArray.car_Speed = (signed char)CarDataArray.rf_rec[3] - 100;
        } else if (CarDataArray.rf_rec[3] >= 150) {
            CarDataArray.car_Speed = (signed char)CarDataArray.rf_rec[3] - 150;
        } else {
            CarDataArray.car_Speed = 0;
        }

        driverSetSteering(CarDataArray.car_Steering);
        driverSetThrottle(CarDataArray.car_Speed);
    }
}
