# ifndef DL_DRIVER_AKTORIK_H_
# define DL_DRIVER_AKTORIK_H_

// defines ---------------------------------
#define ST_MIDDLE 3475
#define ST_MAXLEFT 2675
#define ST_MAXRIGHT 4275

#define STEERING_RANGE_L -100
#define STEERING_RANGE_R 100

#define THROTTLE_RANGE_F 100
#define THROTTLE_RANGE_R -100

#define THROTTLE_STEP 25

#define MaxRPW 2500     //1000 muS
#define MinRPW 5000     //2000 muS
#define MinFPW 7500     //3000 muS
#define MaxFPW 10000    //4000 muS

#define MaxBreak 6250

#define PMW_PERIOD  60  //60Hz


// global functions ---------------------------------
void driverSetSteering(signed char steer_val);
void driverSetThrottle(signed char throttle_val);
void driverSteeringInit(void);
void driverThrottleInit(void);
void driverESCInit();

typedef enum {RUNNING, END} calibration;

# endif /* DRIVER_DRIVER_AKTORIK_H_ */
