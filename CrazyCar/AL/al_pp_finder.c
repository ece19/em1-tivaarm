/*
 * pp_finder.c
 *
 * Peak finder for FFT converted radar data
 */

#include "tiva_headers.h"
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "al_fft.h"
#include "al_heap_sort.h"
#include "al_pp_finder.h"

Peaks peaks[4];

Peaks* peakFinder(int32_t *values)
{

    uint16_t yi, xi = 0;

    while(xi < ((N_SAMPLES/2)- 2)){
        if (values[xi] < values[(xi + 1)] && values[(xi + 1)] > values[(xi + 2)])
        {
            if (peaks[0].val < values[(xi + 1)])
            {
                    peaks[0].val = values[(xi + 1)];
                    peaks[0].index = (xi + 1);

                    heapSort(peaks, PEAKS);

                    xi++;
            }
        }
        xi++;
    }

    //for(yi = 0; yi < PEAKS; yi++){
    //    peaks[yi].freq =  (float)peaks[yi].index * (float)SAMPLE_RATE / (float)N_SAMPLES;
    //}

    return peaks;
}
