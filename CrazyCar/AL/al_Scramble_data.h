/*
 * al_Scramble_data.h
 *
 *  Created on: 21.10.2020
 *      Author: trumm
 */

#ifndef AL_AL_SCRAMBLE_DATA_H_
#define AL_AL_SCRAMBLE_DATA_H_

#define DATA_TYPE int16_t

void Scramble_data(DATA_TYPE *re, DATA_TYPE *im, short len);

#endif
