/*
 * pp_finder.h
 *
 * Peak finder for FFT converted radar data
 */

#ifndef AL_PP_FINDER_H_
#define AL_PP_FINDER_H_

typedef struct {
    uint32_t val;
    uint32_t index;
    float freq;
} Peaks;

Peaks* peakFinder(int32_t *values);

#endif
