/*
 * al_fft.h
 *
 *  Created on: 21.10.2020
 *      Author: Energy and Mobility

 */

#ifndef AL_AL_FFT_H_
#define AL_AL_FFT_H_

#define N_SAMPLES 512   // number of samples
#define SAMPLE_RATE 200 // sample rate in Hz
#define PEAKS 4         // number of taken frequency peaks

void AL_FFT(void);

#endif /* AL_AL_FFT_H_ */