#include "tiva_headers.h"
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "al_heap_sort.h"

void createHeap(Peaks *heap, uint32_t length);
void heapify(Peaks *heap, uint32_t length, uint32_t position);

static void heap_swap(Peaks *heap, uint32_t i, uint32_t j);

void heapSort(Peaks *data, uint32_t length)
{
    uint32_t i;
    createHeap(data, length); // Heap das erste mal nach der Max-Heap-Bedingung ordnen

    for (i = length - 1; i > 0; i--)
    {
        heap_swap(data, i, 0); // "letztes" Element des Heaps mit Wurzel tauschen

        heapify(data, i, 0);
    }
};

// Array zu Max-Heap umformen
void createHeap(Peaks *heap, uint32_t length)
{
    int32_t position;
    for (position = length / 2 - 1; position >= 0; position--)
    {
        heapify(heap, length, position);
    }
}

// Max-Heap-Bedingung herstellen
void heapify(Peaks *heap, uint32_t length, uint32_t position)
{
    uint32_t tmp_pos = position;

    uint32_t left_child;
    uint32_t right_child;

    // Solange ein Kind größer als das Elternelement war
    do
    {
        position = tmp_pos;

        left_child = 2 * position + 1;  // Index für linkes Kind berechnen
        right_child = 2 * position + 2; // Index für rechtes Kind berechnen

        // Falls linkes Kind größer als Elternelement
        if (left_child < length && heap[tmp_pos].val < heap[left_child].val)
        {
            tmp_pos = left_child;
        }

        // Falls rechtes Kind größer als Elternelement oder linkes Kind
        if (right_child < length && heap[tmp_pos].val < heap[right_child].val)
        {
            tmp_pos = right_child;
        }

        // Falls eines der Kinder größer als Elternelement
        if (tmp_pos != position)
        {
            heap_swap(heap, position, tmp_pos); // betroffene Elemente tauschen
        }

    } while (tmp_pos != position);
}

// Elemente an den Stellen i und j tauschen
static void heap_swap(Peaks *heap, uint32_t i, uint32_t j)
{
    Peaks temp_el = heap[i];

    heap[i] = heap[j];
    heap[j] = temp_el;
}
