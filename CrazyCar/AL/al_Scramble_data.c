/*
 * al_Scramble_data.c
 *
 *  Created on: 21.10.2020
 *      Author: trumm
 */

#include "tiva_headers.h"
#include "al_Scramble_data.h"

void Scramble_data(DATA_TYPE *re, DATA_TYPE *im, short len)
{

    DATA_TYPE temp_re; // temporary storage complex variable swaps
    DATA_TYPE temp_im;
    short i; // current sample index
    short j; // bit reversed index
    short k; // used to propagate carryovers

    short N2 = len / 2; // N2 = N >> 1

    // Bit-reversing algorithm. Since 0 -> 0 and N-1 -> N-1
    // under bit-reversal,these two reversals are skipped.

    j = 0;
    for (i = 1; i < (len - 1); i++)
    {
        k = N2;        // k is 1 in msb, 0 elsewhere
        while (k <= j) // Propagate carry to the right if bit is 1
        {
            j = j - k;  // Bit tested is 1, so clear it.
            k = k >> 1; // Carryover binary 1to right one bit.
        }
        j = j + k; // current bit tested is 0, add 1 to that bit
        // Swap samples if current index is less than bit reversed index.
        if (i < j)
        {
            temp_re = re[j];
            temp_im = im[j];

            re[j] = re[i];
            im[j] = im[i];

            re[i] = temp_re;
            im[i] = temp_im;
        }
    }
}