#ifndef AL_HEAP_SORT_H_
#define AL_HEAP_SORT_H_

#include "al_pp_finder.h"

void heapSort(Peaks *data, uint32_t length);

#endif
