/*
 * hal_adc.c
 *
 *  Created on: 14.01.2019
 *      Author: mayerflo
 */

#include "tiva_headers.h"
#include "hal_adc.h"
#include "hal_gpio.h"

#include "../DL/driver_LCD.h"

void timer_SetupADC(void);
void hal_ADCInterruptHandler(void);

extern CarDataCom CarDataArray;
uint32_t timer1A_load;

void HAL_ADC_Init(void)
{

    SysCtlPeripheralEnable(SYSCTL_PERIPH_ADC1); // Enable the ADC0 module
    while (!SysCtlPeripheralReady(SYSCTL_PERIPH_ADC1))
    {
    } // Wait for the ADC0 module to be ready

    SysCtlPeripheralEnable(SYSCTL_PERIPH_TIMER1); // Enable the Timer 1 Module
    while (!SysCtlPeripheralReady(SYSCTL_PERIPH_TIMER1))
    {
    } // Wait for the Timer 1 module to be ready

    timer_SetupADC(); // complete setup for Timer

    //ComparatorConfigure(COMP_BASE, 1,(COMP_TRIG_FALL | COMP_ASRCP_PIN | COMP_OUTPUT_NORMAL));
    ADCSequenceDisable(ADC1_BASE, 1);
    ADCSequenceConfigure(ADC1_BASE, 1, ADC_TRIGGER_TIMER, 0); // setting the priority of the sequence trigger
    ADCReferenceSet(ADC1_BASE, ADC_REF_INT);

    ADCSequenceStepConfigure(ADC1_BASE, 1, 0, ADC_CTL_CH2);                            // IR_FRONT
    ADCSequenceStepConfigure(ADC1_BASE, 1, 1, ADC_CTL_CH5);                            // IR_LEFT
    ADCSequenceStepConfigure(ADC1_BASE, 1, 2, ADC_CTL_CH4);                            // IR_RIGHT
    ADCSequenceStepConfigure(ADC1_BASE, 1, 3, ADC_CTL_IE | ADC_CTL_CH3 | ADC_CTL_END); //VBAT
    ADCIntEnable(ADC1_BASE, 1);
    ADCIntRegister(ADC1_BASE, 1, hal_ADCInterruptHandler);

    ADCSequenceEnable(ADC1_BASE, 1);
    TimerEnable(TIMER1_BASE, TIMER_A);
}

void timer_SetupADC()
{
    TimerConfigure(TIMER1_BASE, TIMER_CFG_PERIODIC);
    timer1A_load = (SysCtlClockGet() / 200);
    TimerLoadSet(TIMER1_BASE, TIMER_A, timer1A_load);
    TimerControlTrigger(TIMER1_BASE, TIMER_A, true);
    TimerIntEnable(TIMER1_BASE, TIMER_TIMA_TIMEOUT);
}

void hal_ADCInterruptHandler(void)
{
    // triggers the ADC via Software , sequence 1
    ADCIntClear(ADC1_BASE, 1);
    ADCSequenceDataGet(ADC1_BASE, 1, adcData);
    HAL_setADC_Data();
}

void HAL_setADC_Data(void)
{
    // Here ADCs get seperated into HIGH and LOW Byte
    int adc_Count = 4; // numbers of ADC channels
    int i = 0;

    while (i < adc_Count)
    {
        CarDataArray.rf_data[i] = (uint16_t)adcData[i];
        //CarDataArray.rf_data[i*2 + 1] = (uint16_t)adcData[i] & 0xFF;
        i++;
    }
}
