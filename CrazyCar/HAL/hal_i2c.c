/*
 * hal_i2c.c
 *
 *  Created on: 02.06.2021
 *      Author: lukas
 */

#include "tiva_headers.h"
#include "hal_i2c.h"

//initialize I2C module 0
//Slightly modified version of TI's example code
void HAL_InitI2C0(void)
{
    SysCtlPeripheralEnable(SYSCTL_PERIPH_I2C0);                         // enable I2C module 0
    while(!SysCtlPeripheralReady(SYSCTL_PERIPH_I2C0)){};

    SysCtlPeripheralReset(SYSCTL_PERIPH_I2C0);                          // reset the module

    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOB);
    while(!SysCtlPeripheralReady(SYSCTL_PERIPH_GPIOB)){};

    GPIOPinConfigure(GPIO_PB2_I2C0SCL);                                 // Configure the pin muxing for I2C0 functions on port B2 and B3.
    GPIOPinConfigure(GPIO_PB3_I2C0SDA);

    GPIOPinTypeI2CSCL(GPIO_PORTB_BASE, GPIO_PIN_2);                     // Select the I2C function for these pins.
    GPIOPinTypeI2C(GPIO_PORTB_BASE, GPIO_PIN_3);

    I2CMasterInitExpClk(I2C0_BASE, SysCtlClockGet(), true);             // 400kbps

    HWREG(I2C0_BASE + I2C_O_FIFOCTL) = 80008000;                        //clear I2C FIFOs
}

// I2CSend
// +++++++++++++++++++++++++++++++++++++++++++++++++++++
// +++++ sends an I2C command to the specified slave +++
// +++++++++++++++++++++++++++++++++++++++++++++++++++++
void I2CSend(uint8_t slave_addr, uint8_t *vars, uint8_t num_of_args)
{
    // Tell the master module what address it will place on the bus when
    // communicating with the slave.
    I2CMasterSlaveAddrSet(I2C0_BASE, slave_addr, false);
    //put data to be sent into FIFO
    I2CMasterDataPut(I2C0_BASE, vars[0]);

    //if there is only one argument, we only need to use the
    //single send I2C function
    if(num_of_args == 1)
    {
        //Initiate send of data from the MCU
        I2CMasterControl(I2C0_BASE, I2C_MASTER_CMD_SINGLE_SEND);
        // Wait until MCU is done transferring.
        while(I2CMasterBusy(I2C0_BASE));
    }

    //otherwise, we start transmission of multiple bytes on the
    //I2C bus
    else
    {
        //Initiate send of data from the MCU
        I2CMasterControl(I2C0_BASE, I2C_MASTER_CMD_BURST_SEND_START);
        while(I2CMasterBusy(I2C0_BASE)); // Wait until MCU is done transferring.

        //send num_of_args-2 pieces of data, using the
        //BURST_SEND_CONT command of the I2C module
        int i = 1;
        while(i < (num_of_args - 1))
        {
            //put next piece of data into I2C FIFO
            I2CMasterDataPut(I2C0_BASE, vars[i]);
            //send next data that was just placed into FIFO
            I2CMasterControl(I2C0_BASE, I2C_MASTER_CMD_BURST_SEND_CONT);
            // Wait until MCU is done transferring.
            while(I2CMasterBusy(I2C0_BASE));
            i++;
        }

        //put last piece of data into I2C FIFO
        I2CMasterDataPut(I2C0_BASE, vars[i]);
        //send next data that was just placed into FIFO
        I2CMasterControl(I2C0_BASE, I2C_MASTER_CMD_BURST_SEND_FINISH);
        // Wait until MCU is done transferring.
        while(I2CMasterBusy(I2C0_BASE));
    }
}

// I2CSendString
// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// +++++ sends an array of data via I2C to the specified slave +++
// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

void I2CReceive(uint8_t slave_addr, uint8_t *vars, uint8_t num_of_args)
{
    // Tell the master module what address it will place on the bus when
    // communicating with the slave.
    I2CMasterSlaveAddrSet(I2C0_BASE, slave_addr, true);
    //put data to be sent into FIFO

    //if there is only one argument, we only need to use the
    //single send I2C function
    if(num_of_args == 1)
    {
        I2CMasterControl(I2C0_BASE, I2C_MASTER_CMD_SINGLE_RECEIVE);  //Initiate receive of data from the MCU
        while(I2CMasterBusy(I2C0_BASE)); // Wait until MCU is done transferring.
        vars[0] = I2CMasterDataGet(I2C0_BASE);
    }
    //otherwise, we start transmission of multiple bytes on the
    //I2C bus
    else
    {
        //Initiate send of data from the MCU
        I2CMasterControl(I2C0_BASE, I2C_MASTER_CMD_BURST_RECEIVE_START);
        while(I2CMasterBusy(I2C0_BASE)); // Wait until MCU is done transferring.
        vars[0] = I2CMasterDataGet(I2C0_BASE);
        int i = 1;
        while(i < (num_of_args - 1))
        {
            I2CMasterControl(I2C0_BASE, I2C_MASTER_CMD_BURST_RECEIVE_CONT); //send next data that was just placed into FIFO
            while(I2CMasterBusy(I2C0_BASE)); // Wait until MCU is done transferring.
            vars[i] = I2CMasterDataGet(I2C0_BASE);
            i++;
        }

        //put last piece of data into I2C FIFO
        I2CMasterControl(I2C0_BASE, I2C_MASTER_CMD_BURST_RECEIVE_FINISH); //send next data that was just placed into FIFO
        while(I2CMasterBusy(I2C0_BASE)); // Wait until MCU is done transferring.
        vars[i] = I2CMasterDataGet(I2C0_BASE);
    }

}

void writeMPUBytes(uint8_t slave_addr, uint8_t reg, uint8_t *vars, uint8_t num_of_args)
{
    uint8_t vals[14];
    uint8_t i = 0;
    vals[0] = reg;

    while(i < num_of_args + 1)
    {
        vals[i+1] = vars[i];
        i++;
    }
    I2CSend(slave_addr, vals, num_of_args + 1);
}

void writeMPUByte(uint8_t slave_addr, uint8_t reg, uint8_t var)
{
    uint8_t vals[16] = {0};
    vals[0] = reg;
    vals[1] = var;

    I2CSend(slave_addr, vals, 2);
}

void getMPUBytes(uint8_t slave_addr, uint8_t reg, uint8_t num_of_args, uint8_t *out)
{
    I2CMasterSlaveAddrSet(I2C0_BASE, slave_addr, false);
    I2CMasterDataPut(I2C0_BASE, reg);
    I2CMasterControl(I2C0_BASE, I2C_MASTER_CMD_BURST_SEND_START);
    while(I2CMasterBusy(I2C0_BASE));

    I2CReceive(slave_addr, out, num_of_args);
}
