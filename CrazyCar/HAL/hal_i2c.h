/*
 * hal_i2c.h
 *
 *  Created on: 02.06.2021
 *      Author: lukas
 */

#ifndef HAL_HAL_I2C_H_
#define HAL_HAL_I2C_H_

void HAL_InitI2C0(void);
void I2CSend(uint8_t slave_addr, uint8_t *vars, uint8_t num_of_args);
void I2CReceive(uint8_t slave_addr, uint8_t *vars, uint8_t num_of_args);
void writeMPUBytes(uint8_t slave_addr, uint8_t reg, uint8_t *vars, uint8_t num_of_args);
void getMPUBytes(uint8_t slave_addr, uint8_t reg, uint8_t num_of_args, uint8_t *out);
void writeMPUByte(uint8_t slave_addr, uint8_t reg, uint8_t var);

#endif /* HAL_HAL_I2C_H_ */
