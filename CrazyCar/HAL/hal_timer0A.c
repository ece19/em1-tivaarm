/*
 * timerA1.c
 *
 *  Created on: 20.10.2017
 *      Author: mayerflo
 */
#include "stdint.h"

#include "tiva_headers.h"
#include "hal_timer0A.h"
#include "hal_gpio.h"

void TIMER0A_INT_HANDLER(void);

uint32_t timer0A_load;
extern CarDataCom CarDataArray;

void HAL_Timer0A_INIT()
{
    SysCtlPeripheralEnable(SYSCTL_PERIPH_TIMER0);
    while(!SysCtlPeripheralReady(SYSCTL_PERIPH_TIMER0)){} // Wait for peripheral to be ready

    TimerConfigure(TIMER0_BASE, TIMER_CFG_PERIODIC);

    timer0A_load = (SysCtlClockGet() / 10); // 10Hz
    TimerLoadSet(TIMER0_BASE, TIMER_A, timer0A_load);

    TimerIntClear(TIMER0_BASE, TIMER_TIMA_TIMEOUT);          // Clear pending interrupts
    TimerIntRegister(TIMER0_BASE, TIMER_A, TIMER0A_INT_HANDLER);       // Register our handler function
    TimerIntEnable(TIMER0_BASE, TIMER_TIMA_TIMEOUT);         // Enabel interrupt

    TimerEnable(TIMER0_BASE, TIMER_A);
}

void TIMER0A_INT_HANDLER(void)
{
    if (GPIOPinRead(GPIO_PORTC_BASE, RPM_SENSOR_DIR) > 0) {
        CarDataArray.RPM_dir = -1;
    } else {
        CarDataArray.RPM_dir = 1;
    }

    CarDataArray.RPM_Speed = (CarDataArray.RPM_ticks*10*10 + CarDataArray.RPM_Speed)>>1; // mm per second
    CarDataArray.RPM_ticks = 0;
    CarDataArray.rf_data[4] = CarDataArray.RPM_Speed;

    TimerIntClear(TIMER0_BASE, TIMER_TIMA_TIMEOUT);
}

