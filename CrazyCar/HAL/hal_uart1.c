/*
 * uart1.c
 *
 *  Created on: 13.04.2021
 *      Author: lukas
 */
#include "hal_uart1.h"
#include "DL/driver_LCD.h"
#include "DL/driver_com.h"

#include "HAL/hal_gpio.h"
#include "tiva_headers.h"
#include "hal_ucs.h"

#define BAUDRATE 230400

extern CarDataCom CarDataArray;

void HAL_UART_Init() {
    SysCtlPeripheralEnable(SYSCTL_PERIPH_UART1);
    while(!SysCtlPeripheralReady(SYSCTL_PERIPH_UART1)) {};

    // Configure UART
    UARTConfigSetExpClk(UART1_BASE,
                        SysCtlClockGet(),
                        BAUDRATE,
                        (UART_CONFIG_WLEN_8 | UART_CONFIG_STOP_ONE | UART_CONFIG_PAR_NONE));

    UARTFIFOLevelSet(UART1_BASE, UART_FIFO_TX1_8, UART_FIFO_RX1_8); // Configure Interrupts
    UARTIntRegister(UART1_BASE, UART1_Int_Handler);                 // register ISR

    // Enable UART/RX Interrupt/UART FIFO
    UARTEnable(UART1_BASE);
    UARTFIFOEnable(UART1_BASE);
    UARTIntEnable(UART1_BASE, UART_INT_RX);
}

void UART1_Transmit(unsigned char* data, unsigned char length) {
    unsigned char i = 0;

    for(i = 0; i < length; i++) {
        UARTCharPut(UART1_BASE, data[i]);
    }
}

void UART1_Int_Handler() {
    unsigned char count = 0;
    unsigned long ulStatus = UARTIntStatus(UART1_BASE, true);

    // Clear interrupt sources
    UARTIntClear(UART1_BASE, ulStatus);

    if(ulStatus & UART_INT_RX) {

        // Receive data
        while(UARTCharsAvail(UART1_BASE)) {
            CarDataArray.rf_rec[count] = UARTCharGetNonBlocking(UART1_BASE);    // read current byte from UART Buffer
            delay_ms(0.1);                                                      // delay for UART Buffer
            count++;
        }

        recData();
        transmitData(CarDataArray.rf_data);
    }
}
