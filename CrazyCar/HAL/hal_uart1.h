/*
 * uart1.h
 *
 *  Created on: 13.04.2021
 *      Author: lukas
 */

#ifndef HAL_HAL_UART1_H_
#define HAL_HAL_UART1_H_

void HAL_UART_Init(void);
void UART1_Transmit(unsigned char* data, unsigned char length);
void UART1_Int_Handler(void);


#endif /* HAL_HAL_UART1_H_ */
