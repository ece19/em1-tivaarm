/*
 * hal_general.c
 *
 *  Created on: 20.09.2017
 *      Author: mayerflo
 */

#include "tiva_headers.h"

#include "hal_general.h"
#include "hal_gpio.h"
#include "hal_ssi_lcd.h"
#include "hal_pwm.h"
#include "hal_timer0A.h"
#include "hal_uart1.h"
#include "hal_ucs.h"
#include "hal_adc.h"
#include "hal_i2c.h"
#include "hal_timer1A.h"

CarDataCom CarDataArray;

void HAL_INIT(void) {
    HAL_GPIO_INIT();
    HAL_UCS_INIT();
    HAL_PWM_Init();
    HAL_Timer0A_INIT();
    HAL_ADC_Init();
    HAL_UART_Init();
    HAL_InitI2C0();
    halTimerA1Init();
    HAL_SSI_Init();

    IntMasterEnable();
}
