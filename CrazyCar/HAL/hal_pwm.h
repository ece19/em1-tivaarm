/*
 * hal_pwm.h
 *
 *  Created on: 09.04.2018
 *      Author: mayerflo
 */

#ifndef HAL_HAL_PWM_H_
#define HAL_HAL_PWM_H_

#define AKTORIK_FREQUENCY 60
#define SONIC_FRQUENCY 40000

#define AKTORIK 1
#define SONIC 0

void HAL_PWM_Init(void);

#endif /* HAL_HAL_PWM_H_ */
