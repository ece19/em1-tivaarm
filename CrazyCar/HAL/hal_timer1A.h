/*
 * hal_timer1A.h
 *
 *  Created on: 02.06.2021
 *      Author: tim
 */

#ifndef HAL_HAL_TIMER1A_H_
#define HAL_HAL_TIMER1A_H_

#define TIMER_FREQ 40000

void halTimerA1Init(void);
void timerA1InterruptHandler(void);

#endif /* HAL_HAL_TIMER1A_H_ */
