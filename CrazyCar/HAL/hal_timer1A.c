/*
 * hal_timer1A.c
 *
 *  Created on: 02.06.2021
 *      Author: tim
 */

#include "stdint.h"

#include "tiva_headers.h"
#include "hal_timer1A.h"
#include "hal_gpio.h"
#include "DL/driver_sonic.h"


uint32_t timer1A_load;

extern short send_counter;
extern short sonic_general_counter;

/*
 * Inital setup for Timer 1A which is used for the SONIC Sensor
 */
void halTimerA1Init(){

    SysCtlPeripheralEnable(SYSCTL_PERIPH_TIMER1);
    while(!SysCtlPeripheralReady(SYSCTL_PERIPH_TIMER1)){}

    TimerConfigure(TIMER1_BASE, TIMER_CFG_PERIODIC);

    timer1A_load = (SysCtlClockGet() / TIMER_FREQ);
    TimerLoadSet(TIMER1_BASE, TIMER_A, timer1A_load);      // config for the period

    TimerIntClear(TIMER1_BASE, TIMER_TIMA_TIMEOUT);                     // Clear pending interrupts
    TimerIntRegister(TIMER1_BASE, TIMER_A, timerA1InterruptHandler);    // Register our handler function
    TimerIntEnable(TIMER1_BASE, TIMER_TIMA_TIMEOUT);                    // Disable interrupt

    TimerEnable(TIMER1_BASE, TIMER_A);

}

void timerA1InterruptHandler(){
    send_counter++;
    sonic_general_counter++;
    TimerIntClear(TIMER1_BASE, TIMER_TIMA_TIMEOUT);
}


