/*
 * hal_pwm.c
 *
 *  Created on: 09.04.2018
 *      Author: mayerflo
 */

/*
* Timer for PWM (Steering and Throttle)
* PWM frequency = 60 Hz
*/
#include "tiva_headers.h"
#include "hal_pwm.h"

#include "DL/driver_aktorik.h"
#include "HAL/hal_gpio.h"

void aktorik_ISR(void);
void sonic_ISR(void);

uint32_t getFrequency(char type);

extern calibration cal;
extern short pulse_counter;

void HAL_PWM_Init()
{
    // dividing the system master clk to a suitable and useable value
    SysCtlPWMClockSet(SYSCTL_PWMDIV_16);

    // // waiting until the peripheral module is ready
    SysCtlPeripheralEnable(SYSCTL_PERIPH_PWM1);
    while (!SysCtlPeripheralReady(SYSCTL_PERIPH_PWM1)){} // ready ?

    // PWM Generator configuration -> Generator 1, Mode Down, No Sync
    PWMGenConfigure(PWM1_BASE, PWM_GEN_1, PWM_GEN_MODE_DOWN | PWM_GEN_MODE_NO_SYNC | PWM_GEN_MODE_DBG_STOP);
    PWMGenConfigure(PWM1_BASE, PWM_GEN_0, PWM_GEN_MODE_DOWN | PWM_GEN_MODE_NO_SYNC | PWM_GEN_MODE_DBG_STOP);

    // Settin the period of the PWM Generator
    PWMGenPeriodSet(PWM1_BASE, PWM_GEN_1, getFrequency(AKTORIK));
    PWMGenPeriodSet(PWM1_BASE, PWM_GEN_0, getFrequency(SONIC));

    // Defining the initial Pulsewidth of Output 2 and 3
    PWMPulseWidthSet(PWM1_BASE, (PWM_OUT_2 | PWM_OUT_3), 0);
    PWMPulseWidthSet(PWM1_BASE, PWM_OUT_1, 0);

    // Handling the ISR with a predefined function
    PWMGenIntRegister(PWM1_BASE, PWM_GEN_1, &aktorik_ISR);
    PWMGenIntRegister(PWM1_BASE, PWM_GEN_0, &sonic_ISR);

    // Setting the trigger mode
    PWMGenIntTrigEnable(PWM1_BASE, PWM_GEN_1, PWM_INT_CNT_ZERO);
    PWMGenIntTrigEnable(PWM1_BASE, PWM_GEN_0, PWM_INT_CNT_ZERO);

    // Enabling the Interrupt for Generator 0/1
    PWMIntEnable(PWM1_BASE, PWM_INT_GEN_1);
    PWMIntEnable(PWM1_BASE, PWM_INT_GEN_0);

    // Defining the Output-state of Bit 2 and 3 for AKTORIK and Bit 1 for SONIC
    PWMOutputState(PWM1_BASE, (PWM_OUT_2_BIT | PWM_OUT_3_BIT), true);
    PWMOutputState(PWM1_BASE, PWM_OUT_1_BIT, true);

    // Enabling the PWM Generator Base 1 -> Generator 1 (AKTORIK) and Generator 0 (SONIC)
    PWMGenEnable(PWM1_BASE, PWM_GEN_1);
    PWMGenEnable(PWM1_BASE, PWM_GEN_0);
}

uint32_t getFrequency(char type)
{

    uint32_t ui32PWMClock = SysCtlClockGet() / 16; //--> set clock to 2,5Mhz

    if (type == 1)
    {
        return (ui32PWMClock / AKTORIK_FREQUENCY) - 1;
    }
    else
    {
        return (ui32PWMClock / SONIC_FRQUENCY) - 1;
    }
}

void aktorik_ISR(void)
{
    PWMGenIntClear(PWM1_BASE, PWM_GEN_1, PWM_INT_CNT_ZERO);
    if (cal == RUNNING)
    {
        pulse_counter++;
    }
}

void sonic_ISR(void)
{
    PWMGenIntClear(PWM1_BASE, PWM_GEN_0, PWM_INT_CNT_ZERO);
}
