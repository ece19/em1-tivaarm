/*
 * hal_adc.h
 *
 *  Created on: 14.01.2019
 *      Author: mayerflo
 */

#ifndef HAL_HAL_ADC_H_
#define HAL_HAL_ADC_H_

uint32_t adcData[4];
bool adcDataReady;

void HAL_ADC_Init(void);
void HAL_setADC_Data(void);

#endif /* HAL_HAL_ADC_H_ */
