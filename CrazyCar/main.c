#include "tiva_headers.h"

#include "HAL/hal_general.h"
#include "HAL/hal_gpio.h"
#include "HAL/hal_ucs.h"
#include "HAL/hal_adc.h"
#include "HAL/hal_ssi_lcd.h"
#include "HAL/hal_uart1.h"

#include "DL/driver_general.h"
#include "DL/driver_LCD.h"
#include "DL/driver_aktorik.h"
#include "DL/driver_com.h"
#include "AL/al_fft.h"

/**
 * main.c
 */

uint32_t sysfrequ;
int duty;

void main(void)
{
    //HAL_INIT();
    //delay_ms(100);
    //driver_INIT();

    sysfrequ = SysCtlClockGet();

    while (1)
    {
        AL_FFT();
    }
}
